let fields = [null, null, null, null, null, null, null, null, null];
let currentPlayer = "circle";

const winningCombinations = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
];

function init() {
  render();
}

function render() {
  container = document.getElementById("fieldContent");
  let tableHtml = `
    <table>`;
  for (let i = 0; i < 3; i++) {
    tableHtml += `<tr>`;
    for (let j = 0; j < 3; j++) {
      let index = i * 3 + j;
      let symbol = "";
      if (fields[index] == "cross") {
        symbol = generateCrossSVG();
      } else if (fields[index] == "zero") {
        symbol = generateCircleSVG();
      }
      tableHtml += `<td onclick="handleClick(this, ${index})">${symbol}</td>`;
    }
    tableHtml += "</tr>";
  }
  tableHtml += `</table>`;
  container.innerHTML = tableHtml;
}

function handleClick(cell, index) {
  if (fields[index] === null) {
    fields[index] = currentPlayer;
    cell.innerHTML =
      currentPlayer === "circle" ? generateCircleSVG() : generateCrossSVG();
    cell.onclick = null;
    if (isGameFinished()) {
      const winCombination = getWinningCombination();
      drawWinningLine(winCombination);
      console.log("Gewinner ist", currentPlayer);
      toggleRestartBtn();
    }
    currentPlayer = currentPlayer === "circle" ? "cross" : "circle";
  }
}

function isGameFinished() {
  return fields.every((field) => field !== null) || getWinningCombination() !== null;
}

function getWinningCombination() {
  for (let i = 0; i < winningCombinations.length; i++) {
    const [a, b, c] = winningCombinations[i];
    if (fields[a] === fields[b] && fields[b] === fields[c] && fields[a] !== null) {
      return winningCombinations[i];
    }
  }
  return null;
}

function toggleRestartBtn (){
  restartBtn = document.getElementById('restartBtn');
  restartBtn.classList.toggle('d-none');
}

function restartGame() {
  fields = [null, null, null, null, null, null, null, null, null];
  toggleRestartBtn();
  render();
}

function generateCircleSVG() {
  const color = "#00B0EF";
  const width = 60;
  const height = 60;
  const stroke = 8;
  const svgHtml = `
    <svg width="${width}" height="${height}">
      <circle cx="${width / 2}" cy="${height / 2}" r="${
    height / 2 - stroke
  }" stroke="${color}" stroke-width="${stroke}" fill="none">
      <animate attributeName="stroke-dasharray" from="0 188.5" to="188.5 0" dur="0.2s" fill="freeze" />
      </circle>
    </svg>`;
  return svgHtml;
}

function generateCrossSVG() {
  const color = "#FFC000";
  const width = 50;
  const height = 50;
  const svgHtml = `
    <svg width="${width}" height="${height}">
      <line x1="5" y1="5" x2="${width - 5}" y2="${height - 5}"
        stroke="${color}" stroke-width="10" stroke-linecap="round">
        <animate attributeName="x2" values="0; ${width}" dur="200ms" />
        <animate attributeName="y2" values="0; ${height}" dur="200ms" />
      </line>
      <line x1="${width - 5}" y1="5" x2="5" y2="${height - 5}"
        stroke="${color}" stroke-width="10" stroke-linecap="round">
        
        <animate attributeName="x2" values="${width}; 0" dur="200ms" />
        <animate attributeName="y2" values="0; ${height}" dur="200ms" />
      </line>
    </svg>
  `;
  return svgHtml;
}

function drawWinningLine(combination) {
  const lineColor = "#ffffff";
  const lineWidth = 5;

  const startCell = document.querySelectorAll(`td`)[combination[0]];
  const endCell = document.querySelectorAll(`td`)[combination[2]];
  const startRect = startCell.getBoundingClientRect();
  const endRect = endCell.getBoundingClientRect();

  const contentRect = document.getElementById("fieldContent").getBoundingClientRect();

  const lineLength = Math.sqrt(
    Math.pow(endRect.left - startRect.left, 2) + Math.pow(endRect.top - startRect.top, 2)
  );

  const lineAngle = Math.atan2(
    endRect.top - startRect.top,
    endRect.left - startRect.left
  );

  const line = document.createElement("div");
  line.style.position = "absolute";
  line.style.width = `${lineLength}px`;
  line.style.height = `${lineWidth}px`;
  line.style.backgroundColor = lineColor;

  line.style.top = `${startRect.top + startRect.height / 2 - lineWidth / 2} px`;
  line.style.left = `${startRect.left + startRect.width / 2} px`;
  line.style.transform = `rotate(${lineAngle}rad)`;
  line.style.top = `${
    startRect.top + startRect.height / 2 - lineWidth / 2 - contentRect.top
  }px`;
  line.style.left = `${startRect.left + startRect.width / 2 - contentRect.left}px`;
  line.style.transform = `rotate(${lineAngle}rad)`;
  line.style.transformOrigin = `top left`;
  document.getElementById("fieldContent").appendChild(line);
}
